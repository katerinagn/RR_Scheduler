This program implements a unix shell with its own task scheduler. It is a round robin scheduler, 
supporting two types of priority: HIGH and LOW. So, if there are processes of HIGH priority, they 
are the ones that are executed using round-robin. On the contrary, if all the processes are of LOW 
priority, they are the ones that are executed round-robin. 

The commands to the shell are:

'p' : the scheduler prints the list of processes being executed in the form: Priority (1 for HIGH, 0 for LOW), PID, Id, Name. 
	  It also prints the running process.
	  
'k' : takes as input the Id of a process and finishes the process.

'e' : takes as input the name of a process and creates a new process under this name.

'q' : the shell is destroyed.

'h' or 'l' : takes as input the Id of a process and makes its priority HIGH or LOW respectively.

The program is executed having as arguments the processes that will be scheduled, for example:
./scheduler prog prog prog

The scheduler is asynchronous, using the POSIX signals SIGALARM and SIGCHLD.