#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <assert.h>

#include <sys/wait.h>
#include <sys/types.h>

#include "proc-common.h"
#include "request.h"

/* Compile-time parameters. */
#define SCHED_TQ_SEC 2                /* time quantum */
#define TASK_NAME_SZ 60               /* maximum size for a task's name */
#define SHELL_EXECUTABLE_NAME "shell" /* executable for shell */


struct proc_list{
        pid_t pid;
        int id;
        char *exec_name;
        struct proc_list *next;
        int prio;
};

typedef struct proc_list proc_list_t;

proc_list_t *current;
proc_list_t *previous;
proc_list_t *proc_list;
int alive;
int nproc;



/* Print a list of all tasks currently being scheduled.  */
static void
sched_print_tasks(void)
{
        int i;
                           
        printf("These are the running tasks:\n");
        proc_list_t *temp = proc_list;

        for (i=0;i<alive;i++){
                printf("%d\t%d\t%d\t%s\n",temp->prio,temp->pid,temp->id,temp->exec_name);
                temp = temp->next;
        }
        printf("The current task is:%s with ID:%d\n",current->exec_name,current->id);
}

/* Send SIGKILL to a task determined by the value of its
 * scheduler-specific id.
 */
static int
sched_kill_task_by_id(int id)
{
        int plithos=0;
        proc_list_t *temp_cur, *temp_prev;
        temp_cur = current;
        temp_prev = previous;
        do {
                temp_cur =  temp_cur->next;
                temp_prev = temp_prev->next;
                plithos++;
        }
        while ((temp_cur->id != id)&&(plithos<=alive));
        if (plithos>alive){
             printf("No task with this id\n");
        }
        else
        kill(temp_cur->pid,SIGKILL);
}


static int
sched_high_priority(int id) //This function is called when the user wants to make a process of high priority.
//What we do is to move this process in the beginning of the list of processes, just after the last existing high priority process. 
{                          
        int plithos=0;     
        int i;
        proc_list_t *temp,*temp1,*temp2,*temp3,*temp4, *temp_cur, *temp_prev;
        temp_cur = current;
        temp_prev = previous;
	if ((current->id !=id)&&(previous->id !=id))  { //find the current position of the process             
        do {                                
                temp_cur = temp_cur->next;
                temp_prev = temp_prev->next;
                plithos++;
        }
        while ((temp_cur->id != id)&&(plithos<=alive));
        if (plithos>alive){ //the given id is invalid
                printf("No task with this id\n");
        }

        else {

                if (temp_cur->prio == 1) //the given task already was of high priority
                        printf("This task is not of low priority\n");

                else {
                        temp2 = (proc_list_t *)malloc(sizeof(proc_list_t));
                        if (proc_list->prio == 0){      //if the first process of the list is of low priority (prio=0), then every process is of low priority
                                						//so the given process has to be first in the list
								temp2->id = temp_cur->id;  
                                temp2->pid = temp_cur->pid; a
                                temp2->prio = 1;
                                temp2->exec_name = temp_cur->exec_name;
                                temp2->next = proc_list;
                                proc_list = temp2; 
                                temp4 = proc_list;
                                for (i=1;i<=alive;i++){
                                        temp4 = temp4->next;
                                }   
                                temp4->next = proc_list; 
                                temp3 = temp_cur;
                                temp_prev->next = temp_cur->next; 
                                free(temp3);                    
                        }
                        else { //there is at least one other process that already is of high priority
                                temp1 = proc_list;
                                temp = proc_list->next;
                                plithos = 0;
                                             
                                while ((temp->prio!=0)&&(plithos<=alive-1)){
                                        temp = temp->next;  
                                        temp1 = temp1->next; 
                                        plithos++;
                                }
                                if (temp_cur != temp){
	                                temp2->id = temp_cur->id; 
	                                temp2->pid = temp_cur->pid; 
	                                temp2->exec_name = temp_cur->exec_name; 
	                                temp2->prio = 1;
	                                temp2->next = temp;
	                                temp1->next = temp2;
	                                temp3 = temp_cur;
	                                temp_prev->next = temp_cur->next;
	                                free(temp3);
                                }
                                else temp_cur->prio=1;

                        }
                }
		}
	}
	else if (current->id==id){  //if the given process is the running one, then this process is the shell,which is already first in the list of processes
								// so just make its prio equal to 1 (high priority)
        if (current->prio == 1) 
                printf("This task is not of low priority\n");
        else current->prio = 1;
	}
	else { 
        do {
                temp_cur = temp_cur->next;
                temp_prev = temp_prev->next;
                plithos++;
        }
        while ((temp_cur->id != id)&&(plithos<=alive));
        if (plithos>alive){
                printf("No task with this id\n");
        }
        else {
                temp2 = (proc_list_t *)malloc(sizeof(proc_list_t));
                                                                  
                temp1 = proc_list;
                temp = proc_list->next;
                if (proc_list->prio != 0){
                        plithos = 0;
                        while ((temp->prio!=0)&&(plithos<=alive-1)){
                                temp = temp->next;
                                temp1 = temp1->next;
                                plithos++;
                        }
                        if (temp_cur != temp){
                                temp2->id = temp_cur->id;
                                temp2->pid = temp_cur->pid;
                                temp2->exec_name = temp_cur->exec_name;
                                temp2->prio = 1;
                                temp2->next = temp;
                                temp1->next = temp2;
                                temp3 = current;
                                while(temp3->next!=previous)
                                        temp3 = temp3->next;
                                temp4 = temp3->next;
                                temp3->next = current; 
                                previous = temp3;  
                                free(temp4);  
                        }
                        else temp_cur->prio=1;
                }
                else {
                                temp2->id = temp_cur->id;
                                temp2->pid = temp_cur->pid;
                                temp2->prio = 1;
                                temp2->exec_name = temp_cur->exec_name;
                                temp2->next = proc_list;
                                proc_list = temp2;
                                temp4 = proc_list;
                                for (i=1;i<=alive;i++){
                                        temp4 = temp4->next;
                                }
                                temp4->next = proc_list;
                                temp3 = temp_cur;
                                temp_prev->next = temp_cur->next;
                                free(temp3);
                }

                 
        }
	}
}


static int sched_low_priority(int id) {		//this function is called when the user wants to make a process of low priority. 
//What we do is to move this process towards the end of the list of processes, just before the first process of low priority that already existed.
                       
        int plithos=0;
        proc_list_t *temp,*temp1, *temp2,*temp3,*temp4, *temp_cur, *temp_prev;
        temp_cur = current;
        temp_prev = previous;
        do {
                temp_cur = temp_cur->next;
                temp_prev = temp_prev->next;
                plithos++;
        }
        while ((temp_cur->id != id)&&(plithos<=alive));
        if (plithos>alive){
                printf("No task with this id\n");
        }
        else {
              if (temp_cur->prio == 0)
                        printf("This task is not of high priority\n");

              else {
                temp=proc_list;
                temp=temp->next;
                if (temp->prio == 0)//the process is the only one that is high, so just change her prio from 1 to 0
                        proc_list->prio = 0;
                else {  //else search for the right position, after the last high process and the first low process
                        temp2= (proc_list_t *)malloc(sizeof(proc_list_t));
                        temp1 = proc_list;
                        temp = proc_list->next;
                        plithos = 0;
                        while ((temp->prio!=0)&&(plithos<alive-1)){
                                temp = temp->next;
                                temp1 = temp1->next;
                                plithos++;
                        }
                    if ((current->id != id)&&(previous->id != id)){ //the process is neither the running process nor the previous one                                                
                        temp2->id = temp_cur->id;       
                        temp2->pid = temp_cur->pid;     
                        temp2->exec_name = temp_cur->exec_name;
                        temp2->prio = 0;
                        temp2->next = temp;
                        temp1->next = temp2;
                        temp3 = temp_cur;
                        temp_prev->next = temp_cur->next;
                        if (temp_cur->id == proc_list->id)
                                proc_list= proc_list->next;
                        free(temp3);
                    }
                    else if(current->id == id){//the process is the running process, we not only have to find the right position, but also 
                        temp2->id = current->id; //run the next process by signaling SIGCONT. 
                        temp2->pid = current->pid; 
                        temp2->exec_name = current->exec_name;
                        temp2->prio = 0;
                        temp2->next = temp;
                        temp1->next = temp2;
                        temp3 = current;
                        previous->next = current->next;
                        current = current->next;
                        proc_list = proc_list->next;
                        free(temp3);
                        printf("CONTEXT SWITCH: Scheduler has chosen the process with ID=%d and PID=%d to run.\n\n", current->id, current->pid);
                        alarm(SCHED_TQ_SEC);
                        if (kill(current->pid,SIGCONT)< 0) {
                                perror("kill");
                                exit(1);
                        }
                    }
                    else{ //the process is the one pointed by previous, so find the right position and also change the pointers previous and previous->next                          // kai episis allazoume tous deiktes previous kai previous->next
                        if (temp_cur != temp1){
                                temp2->id = temp_cur->id;
                                temp2->pid = temp_cur->pid;
                                temp2->exec_name = temp_cur->exec_name;
                                temp2->prio = 1;
                                temp2->next = temp;
                                temp1->next = temp2;
                                temp3 = current;
                                while(temp3->next!=previous)
                                        temp3 = temp3->next;
                
                                temp4 = temp3->next;
                                temp3->next = current;
                                previous = temp3;
                                free(temp4);
                        }
                        else
                                temp_cur->prio = 0; //the process already is in the right position, so just make its prio equal to 0
                   }

                 }


                }
        }
}





static void sched_create_task(char *executable)
{
                proc_list_t *temp;
                temp = (proc_list_t *)malloc(sizeof(proc_list_t));
                nproc++;
                alive++;
                pid_t pid;
                pid=fork();
                if (pid < 0) {
                        perror("fork");
                }
                if (pid ==0){
                        //Child: execve
                        printf("Proccess %d created. PID=%d\n", nproc, getpid());
                        raise(SIGSTOP);
                        //char *executable = argv[i];
                        char *newargv[] = { executable, NULL, NULL, NULL };
                        char *newenviron[] = { NULL };
                        execve(executable, newargv, newenviron);
                        perror("execve");//should never reach here
                        exit(1);
                               
                }
                temp->pid = pid;
                temp->id = nproc;
                temp->exec_name = (char *)malloc(sizeof(char*));
                strcpy(temp->exec_name, executable);
                temp->prio = 0;
                previous->next = temp;
                temp->next = current;


}

/* Process requests by the shell.  */
static int process_request(struct request_struct *rq)
{
        switch (rq->request_no) {
                case REQ_PRINT_TASKS:
                        sched_print_tasks();
                        return 0;

                case REQ_KILL_TASK:
                        return sched_kill_task_by_id(rq->task_arg);

                case REQ_EXEC_TASK:
                        sched_create_task(rq->exec_task_arg);
                        return 0;
                case REQ_HIGH_TASK:
                        sched_high_priority(rq->task_arg);
                        return 0;
                case REQ_LOW_TASK:
                        sched_low_priority(rq->task_arg);
                        return 0;
                default:
                        return -ENOSYS;
        }
}


/*
 * SIGALRM handler
 */
static void
sigalrm_handler(int signum)
{
        if(kill(current->pid, SIGSTOP) <0){
                perror("kill");
                exit(1);
  }
}

/*
 * SIGCHLD handler
 */
static void
sigchld_handler(int signum)
{
        int status;
        pid_t pid;
        proc_list_t *temp, *temp_cur, *temp_prev, *temp1;
        for (;;){
                pid = waitpid(-1 ,&status, WUNTRACED | WNOHANG);
                if (pid < 0) {
                        perror("waitpid");
                        exit(1);
                }
                if (pid == 0) //if no child has changed situation
                        break;
                explain_wait_status(pid, status);
                if (WIFEXITED(status) || WIFSIGNALED(status)) {
                        //child has terminated
                        alive--;
                        if (alive == 0) { //If the proccess was the last one alive
                                printf("\nProcess with ID=%d and PID=%d has finished or has been killed.\n", current->id, current->pid);
                                printf("\nScheduler:  All tasks terminated. Goodbye!\n\n");
                                exit(0);
                        }
                        if (current->pid == pid) { // The current proccess has died. Remove it from the proccess list, find the next one and activate it.
                                printf("\nProcess with ID=%d and PID=%d has finished or has been killed.\n", current->id, current->pid);
                                printf("This process will be erased from the scheduler list and won't be scheduled again\n");
                                temp = current;
                                previous->next = current->next;
                                current = current->next;
                                proc_list = proc_list->next;
                                free(temp);
                                printf("CONTEXT SWITCH: Scheduler has chosen the process with ID=%d and PID=%d to run.\n\n", current->id, current->pid);
                                                       
                                alarm(SCHED_TQ_SEC);
                                if (kill(current->pid,SIGCONT)< 0) {
                                        perror("kill");
                                        exit(1);
                                }
                        }
                        else if (previous->pid==pid){ //The previous process has died
                                printf("\nProcess with ID=%d and PID=%d has finished or has been killed.\n", previous->id, previous->pid);
                                printf("This process will be erased from the scheduler list and won't be scheduled again.\n\n");
                                temp = current;
                                while(temp->next!=previous)
                                        temp = temp->next;
                                temp1 = temp->next;
                                temp->next = current;
                                previous = temp;
                                free(temp1);
                        }
                        else { //Another (not the running) child proccess has died. Remove it from the proccess list
                                temp_cur = current;
                                temp_prev = previous;
                                do {
                                    temp_cur = temp_cur->next;
                                    temp_prev = temp_prev->next;
                                }while (temp_cur->pid != pid);
                                printf("\nProcess with ID=%d and PID=%d has been killed by another process.\n", temp_cur->id, temp_cur->pid);
                                printf("This process will be erased from the scheduler list and won't be scheduled again.\n\n");
                                temp = temp_cur;
                                temp_prev->next = temp_cur->next;
                                free(temp);
                        }
                }
                if (WIFSTOPPED(status)) {
                        //child has stopped
                        printf("\nScheduler has stopped the proccess with ID=%d and PID=%d.\n", current->id, current->pid);
                        if (alive == 1){
                                printf("There is no other ready process:no context switch\n\n");
                        }
                        else { //only if alive>1 there is context switch. if not, the same process should run.
                                if (proc_list->prio == 0){	//if all processes are low then they are executed
                                        previous = current;
                                        current = current->next;
                                }
                                else { 			//else search for the next high process                         
                                        previous = current;
                                        current = current->next;
                                        while (current->prio != 1){
                                                previous = current;
                                                current = current->next;
                                        }
                                }
                                printf("CONTEXT SWITCH: Scheduler has chosen the process with ID=%d and PID=%d to run.\n\n", current->id, current->pid);
                                alarm(SCHED_TQ_SEC);
                                if (kill(current->pid,SIGCONT) < 0){
                                        perror("kill");
                                        exit(1);
                                }
                        }
                }
        }
}



/* Disable delivery of SIGALRM and SIGCHLD. */
static void
signals_disable(void)
{
        sigset_t sigset;

        sigemptyset(&sigset);
        sigaddset(&sigset, SIGALRM);
        sigaddset(&sigset, SIGCHLD);
        if (sigprocmask(SIG_BLOCK, &sigset, NULL) < 0) {
                perror("signals_disable: sigprocmask");
                exit(1);
        }
}

/* Enable delivery of SIGALRM and SIGCHLD.  */
static void
signals_enable(void)
{
        sigset_t sigset;

        sigemptyset(&sigset);
        sigaddset(&sigset, SIGALRM);
        sigaddset(&sigset, SIGCHLD);
        if (sigprocmask(SIG_UNBLOCK, &sigset, NULL) < 0) {
                perror("signals_enable: sigprocmask");
                exit(1);
        }
}


/* Install two signal handlers.
 * One for SIGCHLD, one for SIGALRM.
 * Make sure both signals are masked when one of them is running.
 */
static void
install_signal_handlers(void)
{
        sigset_t sigset;
        struct sigaction sa;

        sa.sa_handler = sigchld_handler;
        sa.sa_flags = SA_RESTART;
        sigemptyset(&sigset);
        sigaddset(&sigset, SIGCHLD);
        sigaddset(&sigset, SIGALRM);
        sa.sa_mask = sigset;
        if (sigaction(SIGCHLD, &sa, NULL) < 0) {
                perror("sigaction: sigchld");
                exit(1);
        }

        sa.sa_handler = sigalrm_handler;
        if (sigaction(SIGALRM, &sa, NULL) < 0) {
                perror("sigaction: sigalrm");
                exit(1);
        }

 /*
  * Ignore SIGPIPE, so that write()s to pipes
  * with no reader do not result in us being killed,
  * and write() returns EPIPE instead.
  */
        if (signal(SIGPIPE, SIG_IGN) < 0) {
                perror("signal: sigpipe");
                exit(1);
                                 
        }
}

static void
do_shell(char *executable, int wfd, int rfd)
{
        char arg1[10], arg2[10];
        char *newargv[] = { executable, NULL, NULL, NULL };
        char *newenviron[] = { NULL };

        sprintf(arg1, "%05d", wfd);
        sprintf(arg2, "%05d", rfd);
        newargv[1] = arg1;
        newargv[2] = arg2;

        raise(SIGSTOP);
        execve(executable, newargv, newenviron);

 /* execve() only returns on error */
        perror("scheduler: child: execve");
        exit(1);
}

/* Create a new shell task.
 *
 * The shell gets special treatment:
 * two pipes are created for communication and passed
 * as command-line arguments to the executable.
 */
static pid_t
sched_create_shell(char *executable, int *request_fd, int *return_fd)
{
        pid_t p;
        int pfds_rq[2], pfds_ret[2];

        if (pipe(pfds_rq) < 0 || pipe(pfds_ret) < 0) {
                perror("pipe");
                exit(1);
        }

        p = fork();
        if (p < 0) {
                perror("scheduler: fork");
                                          
                exit(1);
        }

        if (p == 0) {
        /* Child */
	        close(pfds_rq[0]);
	        close(pfds_ret[1]);
	        do_shell(executable, pfds_rq[1], pfds_ret[0]);
	        assert(0);
        }
        /* Parent */
        close(pfds_rq[1]);
        close(pfds_ret[0]);
        *request_fd = pfds_rq[0];
        *return_fd = pfds_ret[1];
        return p;
}

static void
shell_request_loop(int request_fd, int return_fd)
{
        int ret;
        struct request_struct rq;

        /*
        * Keep receiving requests from the shell.
        */
        for (;;) {
                if (read(request_fd, &rq, sizeof(rq)) != sizeof(rq)) {
                        perror("scheduler: read from shell");
                        fprintf(stderr, "Scheduler: giving up on shell request processing.\n");
                        break;
                }

                signals_disable();
                ret = process_request(&rq);
                signals_enable();

                if (write(return_fd, &ret, sizeof(ret)) != sizeof(ret)) {
                        perror("scheduler: write to shell");
                        fprintf(stderr, "Scheduler: giving up on shell request processing.\n");
                break;
                }
        }
}

int main(int argc, char *argv[])
{

 /* Two file descriptors for communication with the shell */
 	static int request_fd, return_fd;
    nproc = argc-1;
    int i;
    alive = nproc+1;
    if (nproc == 0){
            fprintf(stderr, "There are no tasks. Exiting...\n");
            exit(1);
    }

    pid_t pid;
    int id = 1;


    proc_list = (proc_list_t *)malloc(sizeof(proc_list_t));
    proc_list->pid = sched_create_shell(SHELL_EXECUTABLE_NAME, &request_fd, &return_fd);
    proc_list->exec_name = SHELL_EXECUTABLE_NAME;
    proc_list->id = 0;
    proc_list->prio = 0;

    proc_list_t *temp = proc_list;
    temp->next = (proc_list_t *)malloc(sizeof(proc_list_t));
    temp = temp->next;


    printf("Scheduler's PID is:%d. Creating the processes...\n\n", getpid());
    for(i=1; i < argc; i++){
            pid=fork();
            if (pid < 0) {
                    perror("fork");
            }
            if (pid ==0){
                    //Child: execve
                    printf("Proccess %d created. PID=%d\n", id, getpid());
                    raise(SIGSTOP);
                    char *executable = argv[i];
                    char *newargv[] = { executable, NULL, NULL, NULL };
                    char *newenviron[] = { NULL };
                    execve(executable, newargv, newenviron);
                    perror("execve");//should never reach here
                    exit(1);
            }
            //Parent: add the new proccess to the proccess list
            temp->pid = pid;
            temp->id = id++;
            temp->exec_name = argv[i];
            temp->prio = 0;
            if (i == argc -1){
                    temp->next = proc_list;
            }
            else {
                    temp->next = (proc_list_t *)malloc(sizeof(proc_list_t));
                    temp = temp->next;
            }
	}


 /* Wait for all children to raise SIGSTOP before exec()ing. */
    wait_for_ready_children(nproc);

 /* Install SIGALRM and SIGCHLD handlers. */
    install_signal_handlers();

    current = proc_list;  //First process
    previous = temp;

    alarm(SCHED_TQ_SEC);
    if (kill(current->pid, SIGCONT) < 0){
            perror("kill");
            exit(1);
    }

    shell_request_loop(request_fd, return_fd);

 /* Now that the shell is gone, just loop forever
  * until we exit from inside a signal handler.
  */
    while (pause())
    ;
 /* Unreachable */
    fprintf(stderr, "Internal error: Reached unreachable point\n");
    return 1;
}

